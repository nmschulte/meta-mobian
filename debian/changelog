meta-mobian (0.3.1) unstable; urgency=medium

  * mobian-base: depend on zstd

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 14 Jul 2021 16:58:46 +0200

meta-mobian (0.3.0) unstable; urgency=medium

  * mobian-base: depend on console-setup

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 13 Jan 2021 16:51:27 +0100

meta-mobian (0.2.9) unstable; urgency=medium

  * d/control: replace nemo with portfolio-filemanager

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 12 Jan 2021 01:46:01 +0100

meta-mobian (0.2.8) unstable; urgency=medium

  * d/control: move to salsa and improve package descriptions
  * debian: add salsa-ci
  * d/copyright: update URL and upstream name
  * d/control: reflect mobian-tweaks package split

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 13 Nov 2020 00:45:43 +0100

mobian-meta (0.2.7) unstable; urgency=medium

  * mobian-phosh: depend on phosh-full

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 05 Nov 2020 10:17:51 +0100

mobian-meta (0.2.6) unstable; urgency=medium

  * mobian-base: add rsync
  * mobian-phosh-phone: depend on phosh-phone
  * mobian-phosh-base: depend on phosh-core
  * mobian-phosh-extras: add purple-xmpp-carbons
  * mobian-phosh-games: drop in favor of phosh-games

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 04 Nov 2020 16:08:04 +0100

mobian-meta (0.2.5) unstable; urgency=medium

  * mobian-phosh-phone: depend on gnome-calls and remove wys

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 03 Nov 2020 11:11:42 +0100

mobian-meta (0.2.4) unstable; urgency=medium

  * mobian-phosh-extras: recommend gnome-weather

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 24 Sep 2020 14:59:45 +0200

mobian-meta (0.2.3) unstable; urgency=medium

  * mobian-phosh-extras: make gnome-software a Recommends.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 24 Sep 2020 14:58:14 +0200

mobian-meta (0.2.2) unstable; urgency=medium

  * mobian-phosh-phone: depend on mobian-phone-base

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 22 Sep 2020 15:08:32 +0200

mobian-meta (0.2.1) unstable; urgency=medium

  * mobian-phosh-base: depend on phosh-mobile-tweaks

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Sep 2020 14:11:20 +0200

mobian-meta (0.2.0) unstable; urgency=medium

  [ Sebastian Spaeth ]
  * mobian-phosh-extras: Depend on megapixels instead of pinhole
    pinhole author said this might be a good idea, given the performance benefit
    of megapixels.
    Make it so!

  [ Arnaud Ferraris ]
  * mobian-phosh-extras: don't depend on user-facing apps, recommend them.
    This allows users to uninstall an unwanted app without throwing away the
    whole metapackage

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Sep 2020 10:51:40 +0200

mobian-meta (0.1.9) unstable; urgency=medium

  * mobian-base: add f2fs-tools

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 04 Aug 2020 23:27:36 +0200

mobian-meta (0.1.8) unstable; urgency=medium

  [ Eduardo Minguez Perez ]
  * Added dialog.
    Fixes https://gitlab.com/mobian1/issues/-/issues/34

  [ Arnaud Ferraris ]
  * mobian-phosh-base: add extra useful fonts

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 04 Aug 2020 17:03:46 +0200

mobian-meta (0.1.7) unstable; urgency=medium

  [ Sebastian Spaeth ]
  * mobian-phosh-extras: include pinhole camera app.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 14 Jul 2020 18:35:44 +0200

mobian-meta (0.1.6) unstable; urgency=medium

  * mobian-base: move telephony middleware to new mobian-phone-base package
  * move gnome-clocks and kgx to mobian-phosh-extras.
    These shouldn't break the base packages when uninstalled.
  * mobian-phosh-phone: move feedbackd and gnome-contacts
    feedbackd is used by phosh itself to provide haptic feedback, so we want
    it in mobian-phosh-base.
    gnome-contacts shouldn't be restricted to phones, so it belongs in
    mobian-phosh-extras.
  * mobian-phosh-extras: depend on mobian-phosh-base.
    This will make it easier to provide builds for devices without a modem
    in the future (think PineTab).
  * mobian-base: add chrony for time synchronization.
    Fixes the "2115 bug"
  * mobian-phosh: don't depend on phone apps

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 08 Jul 2020 12:21:19 +0200

mobian-meta (0.1.5) unstable; urgency=medium

  * mobian-devtools: move inetutils-ping to mobian-base
  * mobian-base: add cloud-initramfs-growroot for rootfs resizing

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 27 Jun 2020 17:48:24 +0200

mobian-meta (0.1.4) unstable; urgency=medium

  * mobian-phosh-phone: add gnome-software-plugin-flatpak

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 17 Jun 2020 23:13:14 +0200

mobian-meta (0.1.3) unstable; urgency=medium

  * mobian-phosh-extras: add fwupd as recommended by gnome-software

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 25 May 2020 01:30:53 +0200

mobian-meta (0.1.2) unstable; urgency=medium

  * mobian-phosh-base: add flatpak

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 14 May 2020 02:35:21 +0200

mobian-meta (0.1.1) unstable; urgency=medium

  * mobian-base: install locales-all

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 11 May 2020 14:34:35 +0200

mobian-meta (0.1.0) unstable; urgency=medium

  * Initial release based on `meta-pinephone` v0.3.2

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 07 May 2020 12:02:32 +0200
